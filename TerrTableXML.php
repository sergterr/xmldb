<?php
namespace TerrXml;

use DOMDocument;

/**
 * Class TerrTableXML
 * DOMDocument need declarate in composer.json
 *
 * @since 1.0.2
 */
class TerrTableXML {
    /**
     * XMLDB Tables directory
     * Каталог таблиц XMLDB
     *
     * @var string
     */
    public static $tables_dir = TERRXMLDB;

    /**
     * TerrTableXML
     *
     * @var object
     */
    private $table;

    /**
     * Table name
     *
     * @var string
     */
    private $name;

    /**
     * TerrTableXML constructor.
     *
     * @param $table_name
     */
    public function __construct( $table_name ) {
        // Redefine vars
        $table_name       = (string) $table_name;

        $this->table = self::get( $table_name );
        $this->name  = $table_name;
    }

    /**
     * Configure the settings of XMLDB Tables
     * Настройте параметры таблиц XMLDB
     *
     * @param mixed $setting Setting name
     * @param mixed $value Setting value
     */
    public static function configure( $setting, $value ) {
        if ( property_exists( "table", $setting ) ) {
            self::$$setting = $value;
        }
    }

    /**
     * Table factory.
     *
     *  <code>
     *      $users = TerrTableXML::factory('table_name');
     *  </code>
     *
     * @param $table_name //Table name
     *
     * @return TerrTableXML
     */
    public static function factory( $table_name ) {
        return new TerrTableXML( $table_name );
    }

    /**
     * Check isset table
     *
     * @param $table_name
     *
     * @return bool
     */
    public static function isTableExists( $table_name ) {
        if ( ! empty( self::$tables_dir ) && is_dir( dirname( self::$tables_dir ) ) &&
             is_writable( dirname( self::$tables_dir ) ) &&
             file_exists( self::$tables_dir . '/' . $table_name . '.xml' ) ) {

            return 'exists';
        } elseif ( ! empty( self::$tables_dir ) && ! file_exists( self::$tables_dir . '/' . $table_name . '.xml' ) &&
                   is_dir( dirname( self::$tables_dir ) ) &&
                   is_writable( dirname( self::$tables_dir ) ) ) {

            return 'notexist';
        } else {

            return false;
        }
    }

    /**
     * Create new table
     *
     * XMLDB Table structure:
     *
     *  <?xml version="1.0" encoding="UTF-8"?>
     *  <root>
     *      <options><autoincrement>0</autoincrement></options>
     *      <fields>
     *          <field1/>
     *          <field2/>
     *      </fields>
     *      <record>
     *          <field1>value</field1>
     *          <field2>value</field2>
     *      </record>
     *  </root>
     *
     *  <code>
     *      TerrTableXML::create('table_name', ['field1', 'field2']);
     *  </code>
     *
     * @param $table_name // Table name
     * @param $fields // Fields
     *
     * @return boolean
     */
    public static function create( $table_name, $fields ) {
        // Redefine vars
        $table_name = (string) $table_name;

        if ( self::isTableExists( $table_name ) &&
             'notexist' == self::isTableExists( $table_name ) &&
             isset( $fields ) &&
             is_array( $fields )
        ) {
            // Create table fields
            $_fields = '<fields>';
            foreach ( $fields as $field ) {
                $_fields .= "<$field/>";
            }
            $_fields .= '</fields>';

            // Create new table
            if ( ! empty( self::$tables_dir ) ) {
                return file_put_contents(
                    self::$tables_dir . '/' . $table_name . '.xml',
                    '<?xml version="1.0" encoding="UTF-8"?><root><options><autoincrement>0</autoincrement></options>' . $_fields . '</root>',
                    LOCK_EX
                );
            }
        }

        return false;
    }

    /**
     * Delete table
     *
     *  <code>
     *      TerrTableXML::drop('table_name');
     *  </code>
     *
     * @param $table_name // Table name
     *
     * @return boolean
     */
    public static function drop( $table_name ) {
        // Redefine vars
        $table_name = (string) $table_name;
        // Drop
        if ( ! empty( self::$tables_dir ) && ! is_dir( self::$tables_dir . '/' . $table_name . '.xml' ) ) {
            return unlink( self::$tables_dir . '/' . $table_name . '.xml' );
        }

        return false;
    }

    /**
     * Get table
     *
     *  <code>
     *     $table = TerrTableXML::get('table_name');
     *  </code>
     *
     * @param string $table_name
     *
     * @return array|bool
     */
    public static function get( $table_name = '' ) {
        // Redefine vars
        $table_name = (string) $table_name;

        // Load table
        if (
            ! empty( self::$tables_dir ) &&
            file_exists( self::$tables_dir . '/' . $table_name . '.xml' ) &&
            is_file( self::$tables_dir . '/' . $table_name . '.xml' ) ) {
            return [
                'xml_object'   => TerrXML::loadFile( self::$tables_dir . '/' . $table_name . '.xml' ),
                'xml_filename' => self::$tables_dir . '/' . $table_name . '.xml'
            ];
        } else {
            return false;
        }
    }

    /**
     * Get information about table
     *
     *  <code>
     *      var_dump($users->info());
     *  </code>
     *
     * @return array
     */
    public function info() {
        return [
            'table_name'        => basename( $this->table['xml_filename'], '.xml' ),
            'table_size'        => filesize( $this->table['xml_filename'] ),
            'table_last_change' => filemtime( $this->table['xml_filename'] ),
            'table_last_access' => fileatime( $this->table['xml_filename'] ),
            'table_fields'      => $this->fields(),
            'records_count'     => $this->count(),
            'records_last_id'   => $this->lastId()
        ];
    }

    /**
     * Get table fields
     *
     *  <code>
     *      var_dump($users->fields());
     *  </code>
     *
     * @return array
     */
    public function fields() {
        // Select fields
        $fields_obj = self::_selectOne( $this->table, "fields" );

        $fields = [];

        // Create fields array
        foreach ( $fields_obj as $key => $field ) {
            $fields[] = $key;
        }

        // Return array of fields
        return $fields;
    }

    /**
     * Add new field
     *
     *  <code>
     *      $users->addField('test');
     *  </code>
     *
     * @param $name // Field name
     *
     * @return boolean
     */
    public function addField( $name ) {
        // Redefine vars
        $name = (string) $name;

        // Get table
        $table = $this->table;

        // Select all fields
        $fields = self::_selectOne( $this->table, "fields" );

        // Select current field
        $field = self::_selectOne( $this->table, "fields/{$name}" );

        // If field dosnt exists than create new field
        if ( $field == null ) {

            // Create new field
            $fields->addChild( $name, '' );

            // Save table
            return self::_save( $table );
        } else {
            return false;
        }
    }

    /**
     * Delete field
     *
     *  <code>
     *      $users->deleteField('test');
     *  </code>
     *
     * @param $name // Field name
     *
     * @return boolean
     */
    public function deleteField( $name ) {
        // Redefine vars
        $name = (string) $name;

        // Get table
        $table = $this->table;

        // Select field
        $field = self::_selectOne( $this->table, "fields/{$name}" );

        // If field exist than delete it
        if ( $field != null ) {

            // Delete field
            unset( $field[0] );

            // Save table
            return self::_save( $table );
        } else {
            return false;
        }
    }

    /**
     * Update field
     *
     *  <code>
     *      $users->updateField('login', 'username');
     *  </code>
     *
     * @param $old_name // Old field name
     * @param $new_name // New field name
     *
     * @return boolean
     */
    public function updateField( $old_name, $new_name ) {
        if ( ! empty( self::$tables_dir ) &&
            file_exists( self::$tables_dir . '/' . $this->name . '.xml' ) &&
            is_file( self::$tables_dir . '/' . $this->name . '.xml' )
        ) {
            $table = strtr(
                file_get_contents( self::$tables_dir . '/' . $this->name . '.xml' ),
                [
                    '<' . $old_name . '>'  => '<' . $new_name . '>',
                    '</' . $old_name . '>' => '</' . $new_name . '>',
                    '<' . $old_name . '/>' => '<' . $new_name . '/>'
                ]
            );
            if ( file_put_contents( self::$tables_dir . '/' . $this->name . '.xml', $table ) ) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Check if field exist
     *
     *  <code>
     *      if ($users->existsField('field_name')) {
     *          // do something...
     *      }
     *  </code>
     *
     * @param $name // Name of field to check.
     *
     * @return boolean
     */
    public function existsField( $name ) {
        // Redefine vars
        $name = (string) $name;

        // Select field
        $field = self::_selectOne( $this->table, "fields/{$name}" );

        // Return true or false
        return ( $field == null ) ? false : true;
    }

    /**
     * Add new record
     *
     *  <code>
     *      $users->insert(['login'=>'admin', 'password'=>'pass']);
     *  </code>
     *
     * @param array|null $fields Record fields to insert
     *
     * @return bool
     */
    public function insert( array $fields = null ) {
        $ffilds = $this->get_field_names( $fields );

        $save        = $ffilds['save'];
        $field_names = $ffilds['field_names'];

        // Save record
        if ( $save ) {

            // Find autoincrement option
            $inc = self::_selectOne( $this->table, "options/autoincrement" );

            // Increment
            $inc_upd = $inc + 1;

            // Add record
            $node = $this->table['xml_object']->addChild( TerrXML::safe( $this->name ) );

            // Update autoincrement
            self::_updateWhere( $this->table, "options", [ 'autoincrement' => $inc_upd ] );

            // Add common record fields: id and uid
            $node->addChild( 'id', $inc_upd );
            $node->addChild( 'uid', self::_generateUID() );

            // If exists fields to insert then insert them
            if ( count( $fields ) !== 0 ) {
                $table_fields = array_diff_key( $field_names, $fields );

                // Defined fields
                foreach ( $table_fields as $table_field ) {
                    $node->addChild( $table_field, '' );
                }

                // User fields
                foreach ( $fields as $key => $value ) {
                    $node->addChild( $key, TerrXML::safe( $value ) );
                }
            }

            // Save table
            return self::_save( $this->table );
        } else {
            return false;
        }
    }

    /**
     * Select record(s) in table
     *
     *  <code>
     *      $records = $users->select('[id=2]');
     *      $records = $users->select(null, 'all');
     *      $records = $users->select(null, 'all', null, array('login'));
     *      $records = $users->select(null, 2, 1);
     *  </code>
     *
     * @param null|string $query XPath query
     * @param mixed $row_count Row count. To select all records write 'all'
     * @param null|integer $offset Offset
     * @param array|null $fields Fields
     * @param string $order_by Order by
     * @param string $order Order type
     *
     * @return array
     */
    public function select( $query = null, $row_count = 'all', $offset = null, array $fields = null, $order_by = 'id', $order = 'ASC' ) {
        // Redefine vars
        $query    = ( $query === null ) ? null : (string) $query;
        $offset   = ( $offset === null ) ? null : (int) $offset;
        $order_by = (string) $order_by;
        $order    = (string) $order;

        // Execute query
        if ( $query !== null ) {
            $tmp = $this->table['xml_object']->xpath( '//' . $this->name . $query );
        } else {
            $tmp = $this->table['xml_object']->xpath( $this->name );
        }

        // Init vars
        $data     = [];
        $records  = [];
        $_records = [];

        $one_record = false;

        // If row count is null then select only one record
        // eg: $users->select('[login="admin"]', null);
        if ( $row_count == null ) {
            if ( isset( $tmp[0] ) ) {
                $_records   = $tmp[0];
                $one_record = true;
            }
        } else {

            // If row count is 'all' then select all records
            // eg:
            //     $users->select('[status="active"]', 'all');
            // or
            //     $users->select('[status="active"]');
            foreach ( $tmp as $record ) {
                $data[] = $record;
            }

            $_records = $data;
        }

        // If array of fields is exits then get records with this fields only
        if ( is_array( $fields ) && count( $fields ) > 0 ) {
            if ( count( $_records ) > 0 ) {
                $count        = 0;
                $record_array = [];

                foreach ( $_records as $key => $record ) {
                    foreach ( $fields as $field ) {
                        $record_array[ $count ][ $field ] = (string) $record->$field;
                    }

                    $record_array[ $count ]['id'] = (int) $record->id;

                    if ( $order_by == 'id' ) {
                        $record_array[ $count ]['sort'] = (int) $record->$order_by;
                    } else {
                        $record_array[ $count ]['sort'] = (string) $record->$order_by;
                    }

                    $count ++;
                }

                // Sort records
                $records = self::subvalSort( $record_array, 'sort', $order );

                // Slice records array
                if ( $offset === null && is_int( $row_count ) ) {
                    $records = array_slice( $records, - $row_count, $row_count );
                } elseif ( $offset !== null && is_int( $row_count ) ) {
                    $records = array_slice( $records, $offset, $row_count );
                }
            }
        } else {

            // Convert from XML object to array

            if ( ! $one_record ) {
                $count = 0;
                foreach ( $_records as $xml_objects ) {
                    $vars = get_object_vars( $xml_objects );

                    foreach ( $vars as $key => $value ) {
                        $records[ $count ][ $key ] = (string) $value;

                        if ( $order_by == 'id' ) {
                            $records[ $count ]['sort'] = (int) $vars['id'];
                        } else {
                            $records[ $count ]['sort'] = (string) $vars[ $order_by ];
                        }
                    }

                    $count ++;
                }

                // Sort records
                $records = self::subvalSort( $records, 'sort', $order );

                // Slice records array
                if ( $offset === null && is_int( $row_count ) ) {
                    $records = array_slice( $records, - $row_count, $row_count );
                } elseif ( $offset !== null && is_int( $row_count ) ) {
                    $records = array_slice( $records, $offset, $row_count );
                }
            } else {

                // Single record
                $vars = get_object_vars( $_records[0] );
                foreach ( $vars as $key => $value ) {
                    $records[ $key ] = (string) $value;
                }
            }
        }

        // Return records
        return $records;
    }

    /**
     * Delete current record in table
     *
     *  <code>
     *      $users->delete(2);
     *  </code>
     *
     * @param $id // Record ID
     *
     * @return boolean
     */
    public function delete( $id ) {
        // Redefine vars
        $id = (int) $id;

        // Find record to delete
        $xml_arr = self::_selectOne( $this->table, "//" . $this->name . "[id='" . $id . "']" );

        // If its exists then delete it
        if ( count( $xml_arr ) !== 0 ) {

            // Delete
            unset( $xml_arr[0] );
        }

        // Save table
        return self::_save( $this->table );
    }

    /**
     * Delete with xPath query record in xml file
     *
     *  <code>
     *      $users->deleteWhere('[id=2]');
     *  </code>
     *
     * @param $query // xPath query
     *
     * @return boolean
     */
    public function deleteWhere( $query ) {
        // Redefine vars
        $query = (string) $query;

        // Find record to delete
        $xml_arr = self::_selectOne( $this->table, '//' . $this->name . $query );

        // If its exists then delete it
        if ( count( $xml_arr ) !== 0 ) {

            // Delete
            unset( $xml_arr[0] );
        }

        // Save table
        return self::_save( $this->table );
    }

    /**
     * Update record with xPath query in XML file
     *
     *  <code>
     *      $users->updateWhere('[id=2]', array('login'=>'Admin', 'password'=>'new pass'));
     *  </code>
     *
     * @param $query // XPath query
     * @param array|null $fields Record fields to udpate
     *
     * @return boolean
     */
    public function updateWhere( $query, array $fields = null ) {
        // Redefine vars
        $query = (string) $query;

        $ffilds = $this->get_field_names( $fields );

        $save = $ffilds['save'];

        // Save record
        if ( $save ) {

            // Find record
            $xml_arr = self::_selectOne( $this->table, '//' . $this->name . $query );

            // If its exists then delete it
            if ( count( $fields ) !== 0 ) {
                foreach ( $fields as $key => $value ) {
                    // Else: Strict Mode Error
                    // Creating default object from empty value
                    @$xml_arr->$key = TerrXML::safe( $value, false );
                }
            }

            // Save table
            return self::_save( $this->table );
        } else {
            return false;
        }
    }

    /**
     * Update current record in table
     *
     *  <code>
     *      $users->update(1, array('login'=>'Admin','password'=>'new pass'));
     *  </code>
     *
     * @param $id // Record ID
     * @param array|null $fields Record fields to udpate
     *
     * @return boolean
     */
    public function update( $id, array $fields = null ) {
        // Redefine vars
        $id = (int) $id;

        $ffilds = $this->get_field_names( $fields );
        $save   = $ffilds['save'];

        // Save record
        if ( $save ) {

            // Find record to delete
            $xml_arr = self::_selectOne( $this->table, "//" . $this->name . "[id='" . (int) $id . "']" );

            // If its exists then update it
            if ( count( $fields ) !== 0 ) {
                foreach ( $fields as $key => $value ) {

                    // Delete current
                    unset( $xml_arr->$key );

                    // And add new one
                    $xml_arr->addChild( $key, TerrXML::safe( $value, false ) );
                }
            }

            // Save table
            return self::_save( $this->table );
        } else {
            return false;
        }
    }

    private function get_field_names( $fields ) {
        // Set save flag to true
        $save = true;

        // Foreach fields check is current field already exists
        if ( count( $fields ) !== 0 ) {
            foreach ( $fields as $key => $value ) {
                if ( self::_selectOne( $this->table, "fields/{$key}" ) == null ) {
                    $save = false;
                    break;
                }
            }
        }

        // Get table fields and create fields names array
        $_fields     = self::_selectOne( $this->table, "fields" );
        $field_names = [];
        foreach ( $_fields as $key => $value ) {
            $field_names[ (string) $key ] = (string) $key;
        }

        return [
            'save'        => $save,
            'field_names' => $field_names
        ];
    }

    /**
     * Get last record id
     *
     *  <code>
     *      echo $users->lastId();
     *  </code>
     *
     * @return integer
     */
    public function lastId() {
        $data = $this->table['xml_object']->xpath( "//root/node()[last()]" );

        return (int) $data[0]->id;
    }

    /**
     * Get count of records
     *
     *  <code>
     *      echo $users->count();
     *  </code>
     *
     * @return integer
     */
    public function count() {
        return count( $this->table['xml_object'] ) - 2;
    }

    /**
     * Subval sort
     *
     * @param $a // Array
     * @param $subkey // Key
     * @param string|null $order Order type DESC or ASC
     *
     * @return array
     */
    protected static function subvalSort( $a, $subkey, $order = null ) {
        if ( count( $a ) != 0 || ( ! empty( $a ) ) ) {
            $b = [];
            $c = [];
            foreach ( $a as $k => $v ) {
                $b[ $k ] = function_exists( 'mb_strtolower' ) ?
                    mb_strtolower( $v[ $subkey ] ) :
                    strtolower( $v[ $subkey ] );
            }
            if ( $order == null || $order == 'ASC' ) {
                asort( $b );
            } elseif ( $order == 'DESC' ) {
                arsort( $b );
            }
            foreach ( $b as $key => $val ) {
                $c[] = $a[ $key ];
            }

            return $c;
        }

        return $a;
    }

    /**
     * _selectOne
     *
     * @param $table
     * @param $query
     *
     * @return mixed|null
     */
    protected static function _selectOne( $table, $query ) {
        $tmp = $table['xml_object']->xpath( $query );

        return isset( $tmp[0] ) ? $tmp[0] : null;
    }

    /**
     * _updateWhere
     *
     * @param $table
     * @param $query
     * @param array $fields
     */
    protected static function _updateWhere( $table, $query, $fields = [] ) {
        // Find record to delete
        $xml_arr = self::_selectOne( $table, $query );

        // If its exists then delete it
        if ( count( $fields ) !== 0 ) {
            foreach ( $fields as $key => $value ) {
                $xml_arr->$key = TerrXML::safe( $value, false );
            }
        }

        // Save table
        self::_save( $table );
    }

    /**
     * _generateUID
     */
    protected static function _generateUID() {
        return substr( md5( uniqid( rand(), true ) ), 0, 10 );
    }

    /**
     * Format XML and save
     *
     * @param $table // Array of database name and XML object
     *
     * @return bool
     */
    protected static function _save( $table ) {
        $dom                     = new DOMDocument( '1.0', 'utf-8' );
        $dom->preserveWhiteSpace = false;

        // Save new xml data to xml file only if loadXML successful.
        // Preventing the destruction of the database by unsafe data.
        // note: If loadXML !successful then _save() add&save empty record.
        //       This record cant be removed by delete[Where]() Problem solved by hand removing...
        //       Possible solution: modify delete[Where]() or prevent add&saving of such records.
        // the result now: database cant be destroyed :)
        if ( $dom->loadXML( $table['xml_object']->asXML() ) ) {
            $dom->save( $table['xml_filename'] );

            return true;
        } else {
            return false;
            // report about errors...
        }
    }
}
