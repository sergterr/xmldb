<?php
namespace TerrXml;

use SimpleXMLElement;

/**
 * Class TerrXML
 *
 * @since 1.0.2
 */
class TerrXML {
	/**
	 * Protected constructor since this is a static class.
	 * Защищенный конструктор, так как это статический класс.
	 *
	 * SimpleXMLElement need declarate in composer.json
	 *
	 * @access  protected
	 */
	protected function __construct() {
		// Nothing here
	}

	/**
	 * Create safe xml data. Removes dangerous characters for string.
	 * Создание безопасных данных XML. Удаляет опасные символы для строки.
	 *
	 *  <code>
	 *      $xml_safe = TerrXML::safe($xml_unsafe);
	 *  </code>
	 *
	 * @param $str // String
	 * @param bool $flag Flag
	 *
	 * @return string|string[]|null
	 */
	public static function safe( $str, $flag = true ) {
		// Redefine vars
		$str  = (string) $str;
		$flag = (bool) $flag;

		// Remove invisible chars
		$non_displayables = [
			'/%0[0-8bcef]/',
			'/%1[0-9a-f]/',
			'/[\x00-\x08]/',
			'/\x0b/',
			'/\x0c/',
			'/[\x0e-\x1f]/'
		];
		do {
			$cleaned = $str;
			$str     = preg_replace( $non_displayables, '', $str );
		} while ( $cleaned != $str );

		// htmlspecialchars
		if ( $flag ) {
			$str = htmlspecialchars( $str, ENT_QUOTES, 'utf-8' );
		}

		// Return safe string
		return $str;
	}

	/**
	 * Get XML file
	 * Получить файл XML
	 *
	 *  <code>
	 *      $xml_file = TerrXML::loadFile('path/to/file.xml');
	 *  </code>
	 *
	 * @param $file // File name
	 * @param false $force Method
	 *
	 * @return false|SimpleXMLElement
	 */
	public static function loadFile( $file, $force = false ) {
		// Redefine vars
		$file  = (string) $file;
		$force = (bool) $force;

		// For CMS API XML file force method
		if ( $force ) {
			$xml = file_get_contents( $file );

			return simplexml_load_string( $xml );
		} else {
			if ( file_exists( $file ) && is_file( $file ) ) {
				$xml = file_get_contents( $file );

				return simplexml_load_string( $xml );
			} else {
				return false;
			}
		}
	}

}
