<?php
namespace TerrXml;

/**
 * Class TerrDB
 *
 * @since 1.0.2
 */
class TerrDB {

    /**
     * TerrDB constructor.
     */
    protected function __construct() {
        // Nothing
    }

    /**
     * Define Storage path
     * Определяем Путь к основному хранилищу
     *
     * @param string $storage_dir
     *
     * @return bool
     */
    public static function setStoragePath( $storage_dir = '' ) {
        if ( ! defined( 'DS' ) ) {
            define( 'DS', DIRECTORY_SEPARATOR );
        }
        if ( ! empty( $storage_dir ) && ! defined( 'TERRSTORAGE' ) ) {
            define( 'TERRSTORAGE', $storage_dir );

            return TERRSTORAGE;
        } else {
            return false;
        }
    }

    /**
     * Get STORAGE Path
     * Получаем путь к хранилищу
     *
     * @return false|string
     */
    public static function getStoragePath() {
        if ( ! defined( 'TERRSTORAGE' ) ) {
            return false;
        } else {
            return TERRSTORAGE;
        }
    }

    /**
     * If Isset DB DIR
     * Проверяем существует ли папка с базой данных
     *
     * @param string $db_path
     *
     * @return bool
     */
    public static function checkDatabasePath( $db_path = '' ) {
        if ( ! empty( $db_path ) && is_dir( $db_path ) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Define XMLDB path
     * Определяем Путь к базе данных
     *
     * @param string $db_name
     *
     * @return bool
     */
    public static function setDbPath( $db_name = '' ) {
        $storage_path = self::getStoragePath();
        if ( self::checkDatabasePath( $storage_path . DS . $db_name ) &&
             ! empty( $db_name ) &&
             ! defined( 'TERRXMLDB' )
        ) {
            $db_name = $storage_path . DS . $db_name;
            define( 'TERRXMLDB', $db_name );

            return TERRXMLDB;
        } else {
            return false;
        }
    }

    /**
     * Get XMLDB Path
     * Получаем путь к базе данных
     *
     * @param $db_name
     *
     * @return false|string
     */
    public static function getDbPath( $db_name = '' ) {
        // Redefine vars
        $db_name = (string) $db_name;

        if ( ! empty( $db_name ) ) {
            $storage_path = self::getStoragePath();

            if ( self::checkDatabasePath( $storage_path . DS . $db_name ) ) {
                return $storage_path . DS . $db_name;
            }
        } elseif ( defined( 'TERRXMLDB' ) ) {
            return TERRXMLDB;
        }

        return false;
    }

    /**
     * Configure the settings of XMLDB
     *
     * @param mixed $setting Setting name
     * @param mixed $value Setting value
     */
    public static function configure( $setting, $value ) {
        if ( property_exists( "db", $setting ) ) {
            self::$$setting = $value;
        }
    }

    /**
     * Create new database
     * Создаем новую Базу данных
     *
     * @param string $db_name // Database name
     * @param int $chmod Mode
     *
     * @return bool
     */
    public static function create( $db_name = '', $chmod = 0775 ) {
        $db_path = self::getStoragePath();
        if ( self::checkDatabasePath( $db_path . DS . $db_name ) ) {
            return false;
        }

        return mkdir( $db_path . DS . $db_name, $chmod );
    }


    /**
     * Drop database
     * Удаляем базу данных
     *
     * @param string $db_name // Database name
     *
     * @return bool
     */
    public static function drop( $db_name = '' ) {
        $db_path = self::getDbPath( $db_name );

        if ( ! $db_path ) {
            return false;
        }

        // Drop
        $ob = scandir( $db_path );
        foreach ( $ob as $o ) {
            if ( $o != '.' && $o != '..' ) {
                if ( filetype( $db_path . DS . $o ) == 'dir' ) {
                    self::drop( $db_path . DS . $o );
                } else {
                    unlink( $db_path . DS . $o );
                }
            }
        }
        reset( $ob );
        rmdir( $db_path );

        return true;
    }
}
